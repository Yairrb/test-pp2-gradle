import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.Assertions;

public class HelloWorldTest {


    @Test
    public void assertHelloWorld() {

        HelloWorld hw = new HelloWorld();
        Assertions.assertEquals("HELLO WORLD",hw.sayHello());
    }
}
